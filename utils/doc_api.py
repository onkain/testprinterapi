from utils.request_methods import RequestMethods

base_url = '' # Базовый URL


class PrinterRestApi:

    @staticmethod
    def get_status():
        get_resource = '/api/v1/GetStatus'  # 1
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def get_files():
        get_resource = '/api/v1/GetFiles'  # 2
        full_url = base_url + get_resource
        body = {
            'Dir': '/'  # Полный путь к папке
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def get_aura_queue():
        get_resource = '/api/v1/GetAuraQueue' #3
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def get_printer_info():
        get_resource = '/api/v1/PrinterInfo'  # 4
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def get_file_info():
        get_resource = '/api/v1/FileInfo' #5
        full_url = base_url + get_resource
        body = {
            'PathToFile' : '/robot.gcode'  # Полный путь к файлу и его название
        }
        obtained_result = RequestMethods.post(full_url,body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def get_aura_file_info():
        get_resource = '/api/v1/AuraFileInfo' #6
        full_url = base_url + get_resource
        body = {
            'Jobld' : 3  # ID файла в очереди
        }
        obtained_result = RequestMethods.post(full_url,body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def get_wifi_list():
        get_resource = '/api/v1/WifiList'  # 7
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def start_print():
        get_resource = '/api/v1/StartPrint'  # 8
        full_url = base_url + get_resource
        body = {
            'FileName': '/robot.gcode'  # Полный путь к файлу и название файла
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def start_aura_print():
        get_resource = '/api/v1/StartAuraPrint' #9
        full_url = base_url + get_resource
        body = {
            'file_id' : 3  # ID файла в очереди
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def set_temp():
        get_resource = '/api/v1/SetTemp'  # 10
        full_url = base_url + get_resource
        body = {
            'Tool': 'tool0',  # "chamber" "tool0" "tool1" "bed"
            'Target': 95
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def store_settings():
        get_resource = '/api/v1/StoreSettings'  # 11
        full_url = base_url + get_resource
        body = {
            'Theme': 0,
            'NeedConnectToAura': True,
            'Lang': 0,
            'NetworkName': 'Printer1'
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def load_settings():
        get_resource = '/api/v1/LoadSettings'  # 12
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def set_active_head():
        get_resource = '/api/v1/SetActiveHead'  # 13
        full_url = base_url + get_resource
        body = {
            'ToolId': '0'  # 0 1 2 3 ID активной головы
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def do_calibrate_head_x_y():
        get_resource = '/api/v1/do_calibrate_head_XY'  # 14
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def do_calibrate_buildplate_z():
        get_resource = '/api/v1/do_calibrate_buildplate_z'  # 15
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def do_calibrate_feeder():
        get_resource = '/api/v1/do_calibrate_feeder'  # 16
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def stop_calibrate():
        get_resource = '/api/v1/stop_calibrate'  # 17
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def do_calibrate_build_plate_angle():
        get_resource = '/api/v1/do_calibrate_buildplate_angle'  # 18
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def load_material():
        get_resource = '/api/v1/LoadMaterial'  # 19
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def unload_material():
        get_resource = '/api/v1/UnloadMaterial'  # 20
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def move_x_y_z_e():
        get_resource = '/api/v1/MoveXYZE'  # 21
        full_url = base_url + get_resource
        body = {
            'Axis': 'X',  # X Y Z E
            'Value': 10.01
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def get_eeprom():
        get_resource = '/api/v1/GetEEPROM'  # 22
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    '''@staticmethod
    def set_eeprom():
        get_resource = '/api/v1/SetEEPROM'  # 23
        full_url = base_url + get_resource
        body = {
            # Параметры из таблицы EEPROM
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def start_firmvare_update():
        get_resource = '/api/v1/StartFirmwareUpdate'  # 24
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def check_firmvare_update():
        get_resource = '/api/v1/CheckFirmwareUpdate'  # 25
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def check_host_update():
        get_resource = '/api/v1/CheckHostUpdate'  # 26
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def start_host_update():
        get_resource = '/api/v1/StartHostUpdate'  # 27
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    '''@staticmethod
    def set_head_config():
        get_resource = '/api/v1/MoveXYZE'  # 28
        full_url = base_url + get_resource
        body = [{
            'head_id': 'head_1',  # ID головы (буква или цифра)
            'head_type': 'plastic',  # Тип головы пластик или волокно
            'is_enabled': True  # Разрешено использование этой головы
        }]  # ,{Информация о голове №2}]        
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result'''

    @staticmethod
    def set_admin_password():
        get_resource = '/api/v1/SetAdminPassword'  # 29
        full_url = base_url + get_resource
        body = {
            'CurrentPassword': '0000',
            'NewPassword': '1111'
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def connect_wifi():
        get_resource = '/api/v1/ConnectWifi'  # 30
        full_url = base_url + get_resource
        body = {
            'SSID': 'worknet_2',
            'Password': '12345678'
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def wifi_disconnect():
        get_resource = '/api/v1/WifiDisconnect'  # 31
        full_url = base_url + get_resource
        obtained_result = RequestMethods.get(full_url)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def set_job_state():
        get_resource = '/api/v1/SetJobState'  # 32
        full_url = base_url + get_resource
        body = {
            'JobState': 0
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def set_feed_rate():
        get_resource = '/api/v1/SetFeedRate'  # 33
        full_url = base_url + get_resource
        body = {
            'FeedRate': 100
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def set_feed_rate():
        get_resource = '/api/v1/SetFeedrate'  # 33
        full_url = base_url + get_resource
        body = {
            'FeedRate': 100
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result

    @staticmethod
    def set_plastic_flow():
        get_resource = '/api/v1/SetPlasticFlow'  # 34
        full_url = base_url + get_resource
        body = {
            'PlasticFlow': 100,
            'Tool': 'T0'  # T0 T1
        }
        obtained_result = RequestMethods.post(full_url, body)
        print(full_url)
        print(obtained_result.text)
        return obtained_result
