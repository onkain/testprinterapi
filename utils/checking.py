from requests import Response
import json


# Методы для валидации данных из запросов
class Checking():

    @staticmethod
    def check_status_code(response: Response, status_code):  # Проверка на соответствие статус кода с указанным
        assert status_code == response.status_codeа
        if response.status_code == status_code:
            print('\nУспех. Status code = ' + str(response.status_code))
        else:
            print('\nОшибка. Status code = ' + str(response.status_code))



'''
    @staticmethod 
    def check_json_token(response: Response, expected_value): # Проверка обязательный полей
        token = json.loads(response.text)
        assert list(token) == expected_value
        print('Все поля присутствуют')
'''
