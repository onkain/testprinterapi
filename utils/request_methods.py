import requests
from utils.logger import Logger

class RequestMethods:
    headers = {'Content-Type': 'application/json'}
    cookies = ''

    @staticmethod
    def get(url):
        Logger.add_request(url,method='GET')
        result = requests.get(url, headers=RequestMethods.headers, cookies=RequestMethods.cookies)
        Logger.add_response(result)
        return result

    @staticmethod
    def post(url, body):
        Logger.add_request(url, method='POST')
        result = requests.post(url, json=body, headers=RequestMethods.headers, cookies=RequestMethods.cookies)
        Logger.add_response(result)
        return result
