import json
from utils.doc_api import PrinterRestApi
from requests import Response
from utils.checking import Checking

class TestApi:


    def test_get_status(self):
        print('\nТест 1. Получить статус принтера')
        result: Response = PrinterRestApi.get_status()
        get_result_json = result.json()
        Checking.check_status_code(result, 200) # Проверка на соответствие с указанным кодом (200)


    def test_get_files(self):
        print('\nТест 2. Получить список файла из каталога data/gcode')
        result: Response = PrinterRestApi.get_files()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_get_aura_queue(self):
        print('\nТест 3. Заглушка. Получить список очереди с Aura.Connect')
        result: Response = PrinterRestApi.get_aura_queue()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_get_printer_info(self):
        print('\nТест 4. Получить информацию о принтере')
        result: Response = PrinterRestApi.get_printer_info()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_get_file_info(self):
        print('\nТест 5. Получить информацию о G-code файле с usb флешки')
        result: Response = PrinterRestApi.get_file_info()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    '''def test_get_aura_file_info(self):
        print('\nТест 6. Получить информацию о G-code файле с Aura.Connect')
        result: Response = PrinterRestApi.get_aura_file_info()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_get_wifi_list(self):
        print('\nТест 7. Получить список доступных Wifi сетей 5.1')
        result: Response = PrinterRestApi.get_wifi_list()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_start_print(self):
        print('\nТест 8. Отправка файла на печать c USB 0,1')
        result: Response = PrinterRestApi.start_print()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_start_aura_print(self):
        print('\nТест 9. Заглушка. Отправка на печать с Aura.Connect')
        result: Response = PrinterRestApi.start_aura_print()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_set_temp(self):
        print('\nТест 10. Установить температуру устройства “tool” 0,3')
        result: Response = PrinterRestApi.set_temp()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_store_settings(self):
        print('\nТест 11. Сохранить настройки веба')
        result: Response = PrinterRestApi.store_settings()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)


    def test_load_settings(self):
        print('\nТест 12. Информация по теме интерфейса 0,4')
        result: Response = PrinterRestApi.load_settings()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_set_active_head(self):
        print('\nТест 13. Установка активной головы')
        result: Response = PrinterRestApi.set_active_head()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_do_calibrate_head_x_y(self):
        print('\nТест 14. Калибровка активной головы XY 1.1.1')
        result: Response = PrinterRestApi.do_calibrate_head_x_y()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_do_calibrate_buildplate_z(self):
        print('\nТест 15. Калибровка стола Z 1.1.2')
        result: Response = PrinterRestApi.do_calibrate_buildplate_z()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_do_calibrate_feeder(self):
        print('\nТест 16. Калибровка фидера активной головы 1.1.3')
        result: Response = PrinterRestApi.do_calibrate_feeder()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_stop_calibrate(self):
        print('\nТест 17. Остановка калибровки 1.1.1 - 1.1.3')
        result: Response = PrinterRestApi.stop_calibrate()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_do_calibrate_buildplate_angle(self):
        print('\nТест 18. Калибровка угла стола  1.2.1')
        result: Response = PrinterRestApi.do_calibrate_buildplate_angle()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_load_material(self):
        print('\nТест 19. Загрузка материала 2.1 - 2.4')
        result: Response = PrinterRestApi.load_material()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_unload_material(self):
        print('\nТест 20. Выгрузка материала 2.1 - 2.4')
        result: Response = PrinterRestApi.unload_material()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_move_x_y_z_e(self):
        print('\nТест 21. Перемещение активной головы, стола, фидера 3.0')
        result: Response = PrinterRestApi.move_x_y_z_e()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_get_eeprom(self):
        print('\nТест 22. Запрос параметров из таблицы EEPROM 4.1, 4.2')
        result: Response = PrinterRestApi.get_eeprom()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_set_eeprom(self):
        print('\nТест 23. Установка параметров из таблицы EEPROM 4.1, 4.2')
        result: Response = PrinterRestApi.set_eeprom()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_start_firmvare_update(self):
        print('\nТест 24. Установка обновления прошивки')
        result: Response = PrinterRestApi.start_firmvare_update()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_check_firmvare_update(self):
        print('\nТест 25. Проверка наличия файлов на sd-карте')
        result: Response = PrinterRestApi.check_firmvare_update()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_check_host_update(self):
        print('\nТест 26. Проверка наличия файлов на флешке и актуальность обновления хоста')
        result: Response = PrinterRestApi.check_host_update()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_start_host_update(self):
        print('\nТест 27. Установка обновления хоста')
        result: Response = PrinterRestApi.start_host_update()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    '''def test_set_head_config(self):
        print('\nТест 28. Смена печатающей головы 4.5')
        result: Response = PrinterRestApi.set_head_config()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)'''

    def test_set_admin_password(self):
        print('\nТест 29. Смена пароля администратора')
        result: Response = PrinterRestApi.set_admin_password()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_connect_wifi(self):
        print('\nТест 30. Подключение к Wi-Fi сети')
        result: Response = PrinterRestApi.connect_wifi()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_wifi_disconnect(self):
        print('\nТест 31. Отключиться от Wi-Fi')
        result: Response = PrinterRestApi.wifi_disconnect()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_set_job_state(self):
        print('\nТест 32. Отмена печати 8.1, 8.2.1')
        result: Response = PrinterRestApi.set_job_state()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_set_feed_rate(self):
        print('\nТест 33. Установка скорости печати')
        result: Response = PrinterRestApi.set_feed_rate()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)

    def test_set_plastic_flow(self):
        print('\nТест 34. Установка коэффицента экструзии пластика')
        result: Response = PrinterRestApi.set_plastic_flow()
        get_result_json = result.json()
        Checking.check_status_code(result, 200)
